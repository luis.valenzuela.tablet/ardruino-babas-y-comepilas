#include <AFMotor.h>

AF_DCMotor motor1(1);/*controla el movimiento de la base de babas*/
AF_DCMotor motor2(2);/*controla em movimiento de la zona inicial de babas*/
AF_DCMotor motor3(3);/*controla que babas tenga su garra mas arriba o mas abajo en la zona media*/
AF_DCMotor motor4(4);/*controla que tan inclinado esta babas en la zona superior*/
AF_DCMotor motor5(5);/*mano de babas controla si agarra o suelta las cosas*/

void setup() {
 motor1.setSpeed(255);
 motor2.setSpeed(255);
 motor3.setSpeed(255);
 motor4.setSpeed(255);
 motor5.setSpeed(255);

}

void loop() {

//CICLO DE INICIO DEL MOVIMIENO 

motor1.run(FORWARD);
delay(3000);
motor1.run(RELEASE);
delay(1000);

motor2.run(FORWARD);
delay(2000);
motor2.run(RELEASE);
delay(1000);
  
motor3.run(FORWARD);
delay(2000);
motor3.run(RELEASE);
delay(1000);

motor4.run(FORWARD);      
delay(6000);
motor4.run(RELEASE);
0delay(1000);

motor5.run(FORWARD);      
delay(2000);
motor5.run(RELEASE);
0delay(1000);


//FIN DEL MOVIMIENTO E INICIO A VOLVER AL PUNTO INICIAL 


motor1.run(BACKWARD);
delay(3000);
motor1.run(RELEASE);
delay(1000);

motor5.run(BACKWARD);
delay(2000);
motor5.run(RELEASE);
delay(1000);

motor5.run(BACKWARD);
delay(2000);
motor5.run(RELEASE);
delay(1000);

motor3.run(BACKWARD);
delay(6000);
motor3.run(RELEASE);
delay(1000);

motor2.run(BACKWARD);
delay(2000);
motor2.run(RELEASE);
delay(1000);


}
